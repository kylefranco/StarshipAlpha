Starship Alpha
===========

Version 0.3

Author: Kyle Franco

kylefranco2@gmail.com

Details
===========
Starship Alpha is a text-puzzle game that has been written
entirely in Java. The Alpha text game engine is the platform 
on which this game runs. For licensing and legal information 
which pertains to the game engine, please visit the gitlab link
included below.


Documentation
===========
In depth documentation of the source code for this progra
is available either under the "docs" folder which is bundled
with this program. It can also be found at the following gitlab link
https://gitlab.com/kylefranco/StarshipAlpha


Licenses, and other legal information
===========
The source code for the game "Starship Alpha"
is proprietary and may not be altered or used in any way
without the express permission of the original author of the
program. The engine that the Starship Alpha game runs on
is the Alpha Text Game Engine, the source code for this engine
is under a GPL Version 2 licensing agreement. The agreement should
have been bundled with a copy of the Alpha Text Game Engine
or can be obtained from the gitlab link below.

