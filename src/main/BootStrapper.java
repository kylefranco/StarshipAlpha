package main;

import main.LanguageParser.Dictionary;
import main.Actors.Player;

/**
 * Created by Kyle on 4/18/2017.
 */
public class BootStrapper {

    /**
     * boot up all the different parts of the program
     * 1. boot dictionary
     * 2. boot items and objects
     * 3. boot player and inventory
     * 4. boot world state
     */

    static Dictionary dictionary = new Dictionary();
    static GameWorld theWorld = new GameWorld();
    static Player playerCharacter = new Player();

    public void bootUpGame()
    {
        dictionary.bootDictionary();
        theWorld.createWorld();
    }
}
