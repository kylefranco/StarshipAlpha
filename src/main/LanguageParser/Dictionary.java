package main.LanguageParser;

import com.sun.corba.se.spi.ior.IORTemplate;
import main.Items.Item;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Kyle on 4/18/2017.
 */
public class Dictionary {

    private static ArrayList<String> Actions = new ArrayList<>();
    private static ArrayList<String> Items = new ArrayList<>();
    private static ArrayList<String> Objects = new ArrayList<>();

    private void populateActionList()
    {
        Actions.add("look");
        Actions.add("walk");
        Actions.add("move");
        Actions.add("combine");
        Actions.add("get");
        Actions.add("take");
        Actions.add("grab");
        Actions.add("open");
        Actions.add("enter");
        Actions.add("investigate");
        Actions.add("search");
        Actions.add("run");
        Actions.add("crawl");
        Actions.add("pick");
        Actions.add("put");
        Actions.add("view");
        Actions.add("insert");
        Actions.add("remove");
        Actions.add("inspect");
        Actions.add("press");
        Actions.add("talk");
        Actions.add("hit");
        Actions.add("smack");
        Actions.add("smash");
        Actions.add("whack");
        Actions.add("type");
    }

    private void populateItemList()
    {
        Items.add("wrench");
        Items.add("marble");
        Items.add("gum");
        Items.add("gloves");
        Items.add("tape");
        Items.add("book");
        Items.add("battery");
        Items.add("torch");
        Items.add("map");
    }

    private void populateObjectList()
    {
        Objects.add("reactor");
        Objects.add("computer");
        Objects.add("monitor");
        Objects.add("generator");
        Objects.add("life support");
    }

    public void bootDictionary()
    {
        populateActionList();
        populateItemList();
        populateObjectList();
    }

    public boolean checkInput(String[] stringArray)
    {
        boolean isValid = false;

        return isValid;
    }
}
