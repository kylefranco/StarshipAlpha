package main;

import main.Actors.*;
import main.Areas.Area;
import main.Items.Item;
import main.Items.ObtainableItem;

import java.util.Vector;
//import main.Areas.Area;
//import main.Items.Item;
//import main.Actors.*;

/**
 * Created by ultrapepe on 9/6/2015.
 */
public class GameWorld {
    private static Vector obtainableItemVector;
    private static Vector areaVector;



    public void createWorld()
    {
        Area CryoStasis = new Area();
        Area MainHallNorth = new Area();
        Area MainHallCenter = new Area();
        Area MainHallSouth = new Area();
        Area Bridge = new Area();
        Area LifeSupportRoom = new Area();
        Area PowerRoom = new Area();
        Area ReactorBay = new Area();

        Item Battery = new ObtainableItem();
        Item Gum = new ObtainableItem();
        Item Marble = new ObtainableItem();
        Item Reactor = new ObtainableItem();
        Item Tape = new ObtainableItem();
        Item WeldingTorch = new ObtainableItem();
        Item Wire = new ObtainableItem();
        Item Wrench = new ObtainableItem();

        areaVector.add(CryoStasis);
        areaVector.add(MainHallNorth);
        areaVector.add(MainHallCenter);
        areaVector.add(MainHallSouth);
        areaVector.add(Bridge);
        areaVector.add(LifeSupportRoom);
        areaVector.add(PowerRoom);
        areaVector.add(ReactorBay);

        obtainableItemVector.add(Battery);
        obtainableItemVector.add(Gum);
        obtainableItemVector.add(Marble);
        obtainableItemVector.add(Reactor);
        obtainableItemVector.add(Tape);
        obtainableItemVector.add(WeldingTorch);
        obtainableItemVector.add(Wire);
        obtainableItemVector.add(Wrench);

        // set defaults for player
        Player player = new Player();
        player.setHealth(0);
        player.setLocation("cryostasis");
        player.setName("player");

    }
}
