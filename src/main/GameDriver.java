package main;

import java.util.Scanner;

/**
 * Created by ultrapepe on 7/20/2015.
 */
public class GameDriver {
    static BootStrapper gameBooter = new BootStrapper();
    public static String userInput;
    static Scanner keyboard = new Scanner(System.in);

    public static void main(String [] args) {
        gameBooter.bootUpGame(); //run game initialization process

        String introductionInformation = "Starship Alpha: Version 0.1\nCreated by: Kyle Franco\n" +
                "To start a new game type 'new'\nTo load a previously saved game type " +
                "'load YourSavedGameNameHere'\nType 'help' to view the help file";
        System.out.print(introductionInformation);

        userInput = keyboard.nextLine();

        while(true) {
            retrieveStory();

        }
    }

    public static void retrieveStory()
    {

    }
}
