package main.Actors;

/**
 * Created by ultrapepe on 7/20/2015.
 */
public class Actor {
    private int health;
    private String name;
    private String location;

    public void setHealth(int hp)
    {
        health = hp;
    }

    public void setName(String newName)
    {
        name = newName;
    }

    public void setLocation(String newLocation)
    {
        location = newLocation;
    }
}
