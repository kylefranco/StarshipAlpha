package main.Areas;

/**
 * Created by ultrapepe on 7/20/2015.
 */
public class Area {
    private int location;
    private String locationName;
    private String locationDescription;

    public int getLocation()
    {
        return location;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public String getLocationDescription()
    {
        return locationDescription;
    }

}
