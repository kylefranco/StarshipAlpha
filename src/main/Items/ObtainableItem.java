package main.Items;

/**
 * Created by ultrapepe on 4/21/2017.
 */

import java.util.ArrayList;

/**
 * Obtainable items are items that can be placed into a players inventory
 */

public class ObtainableItem extends Item {
    boolean isObtainable = true;
    ArrayList obtainableItems = new ArrayList();

    public void wrenchItem()
    {
        setItemDescription("Null");
        setLocation("NULL");
        setItemName("Wrench");
    }

    public void marbleItem()
    {
        setItemName("Marble");
        setItemDescription("Null");
        setLocation("Null");
    }

    public void chewedGumItem()
    {
        setItemName("Chewed Gum");
        setItemDescription("Null");
        setLocation("Null");
    }
}
