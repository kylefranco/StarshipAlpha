package main.Items;

/**
 * Created by ultrapepe on 7/20/2015.
 */
public class Item {
    private String itemName;
    private String itemDescription;
    private String location;

    public void setItemName(String name)
    {
        itemName = name;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemDescription(String description)
    {
        itemDescription = description;
    }

    public String getItemDescription()
    {
        return itemDescription;
    }

    public void setLocation(String local)
    {
        location = local;
    }

    public String getLocation()
    {
        return location;
    }
}
