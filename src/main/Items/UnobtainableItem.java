package main.Items;

/**
 * Created by ultrapepe on 4/21/2017.
 */

/**
 * Unobtainable items are objects in the world, such as level assets
 * IE: a rock, a computer, etc
 */
public class UnobtainableItem extends Item {
    boolean isObtainable = false;

}
